# Pagure Exporter As A Service

## HELP NEEDED

If you can help create a free robot account on GitLab, please reach out to [t0xic0der@fedoraproject.org](mailto:t0xic0der@fedoraproject.org).

## Promotion

Want to reliably transfer massive repositories from Pagure to GitLab without worrying about unstable internet connection?

Let "**Pagure Exporter As A Service**" help you out.

## Preparation

### At the source namespace

At **Pagure**'s end, ensure that the account `t0xic0der` has been provided elevated access to the already existing source namespace.

### At the destination namespace

At **GitLab**'s end, ensure that the account `gridhead` has been provided elevated access to the newly created destination namespace.

## Execution

Please follow the usage instructions provided below to use the service.

### Transfer issue tickets

1. Place the source namespace, destination namespace and datetime information to the environment variables.
   ```
   export SRCEREPO_PAGURE="<ADD YOUR SOURCE NAMESPACE IDENTIFIER HERE>"
   ```
   ```
   export DESTREPO_GITLAB="<ADD YOUR DESTINATION NAMESPACE IDENTIFIER HERE>"
   ```
   ```
   export AUTOPAGEXP_DATE="$(date +%Y%m%d-%H%M%S%Z)"
   ```

2. Clone the fork of the repository to your local storage and make a copy of the `tkts.yaml` file in the `automate` directory.
   ```
   git clone git@gitlab.com:<YOUR USERNAME>/autopagexp.git
   ```
   ```
   cd autopagexp
   ```
   ```
   mkdir "automate/$AUTOPAGEXP_DATE"
   ```
   ```
   cp template/tkts.yaml automate/$AUTOPAGEXP_DATE/tkts.yaml
   ```

3. Add the source namespace, destination namespace and datetime information to the created file and edit the specific options.
   ```
   sed -i "s~STOW_SRCE_HERE~$SRCEREPO_PAGURE~g" "autopagexp/automate/$AUTOPAGEXP_DATE/tkts.yaml"
   ```
   ```
   sed -i "s~STOW_DEST_HERE~$DESTREPO_GITLAB~g" "autopagexp/automate/$AUTOPAGEXP_DATE/tkts.yaml"
   ```
   ```
   sed -i "s~STOW_DATE_HERE~$AUTOPAGEXP_DATE~g" "autopagexp/automate/$AUTOPAGEXP_DATE/tkts.yaml"
   ```

4. Commit the changes to a newly created branch of the local directory and open a pull request to the repository.
   ```
   git checkout -b "export-tkts-$AUTOPAGEXP_DATE-$SRCEREPO_PAGURE-to-$DESTREPO_GITLAB"
   ```
   ```
   git add "automate/$AUTOPAGEXP_DATE/tkts.yaml"
   ```
   ```
   git commit -sm "[$AUTOPAGEXP_DATE] Export issue tickets from '$SRCEREPO_PAGURE' to '$DESTREPO_GITLAB'"
   ```
   ```
   git push origin "export-tkts-$AUTOPAGEXP_DATE-$SRCEREPO_PAGURE-to-$DESTREPO_GITLAB"
   ```

5. After the pull request is approved and merged, the running job can be found under the name `transfer_resource_tkts_$AUTOPAGEXP_DATE`.

### Transfer repository assets

1. Place the source namespace, destination namespace and datetime information to the environment variables.
   ```
   export SRCEREPO_PAGURE="<ADD YOUR SOURCE NAMESPACE IDENTIFIER HERE>"
   ```
   ```
   export DESTREPO_GITLAB="<ADD YOUR DESTINATION NAMESPACE IDENTIFIER HERE>"
   ```
   ```
   export AUTOPAGEXP_DATE="$(date +%Y%m%d-%H%M%S%Z)"
   ```

2. Clone the fork of the repository to your local storage and make a copy of the `repo.yaml` file in the `automate` directory.
   ```
   git clone git@gitlab.com:<YOUR USERNAME>/autopagexp.git
   ```
   ```
   cd autopagexp
   ```
   ```
   mkdir "autopagexp/automate/$AUTOPAGEXP_DATE"
   ```
   ```
   cp autopagexp/template/repo.yaml autopagexp/automate/$AUTOPAGEXP_DATE/repo.yaml
   ```

3. Add the source namespace, destination namespace and datetime information to the created file and edit the specific options.
   ```
   sed -i "s~STOW_SRCE_HERE~$SRCEREPO_PAGURE~g" "autopagexp/automate/$AUTOPAGEXP_DATE/repo.yaml"
   ```
   ```
   sed -i "s~STOW_DEST_HERE~$DESTREPO_GITLAB~g" "autopagexp/automate/$AUTOPAGEXP_DATE/repo.yaml"
   ```
   ```
   sed -i "s~STOW_DATE_HERE~$AUTOPAGEXP_DATE~g" "autopagexp/automate/$AUTOPAGEXP_DATE/repo.yaml"
   ```

4. Commit the changes to a newly created branch of the local directory and open a pull request to the repository.
   ```
   git checkout -b "export-repo-$AUTOPAGEXP_DATE-$SRCEREPO_PAGURE-to-$DESTREPO_GITLAB"
   ```
   ```
   git add "automate/$AUTOPAGEXP_DATE/repo.yaml"
   ```
   ```
   git commit -sm "[$AUTOPAGEXP_DATE] Export repository assets from '$SRCEREPO_PAGURE' to '$DESTREPO_GITLAB'"
   ```
   ```
   git push origin "export-repo-$AUTOPAGEXP_DATE-$SRCEREPO_PAGURE-to-$DESTREPO_GITLAB"
   ```

5. After the pull request is approved and merged, the running job can be found under the name `transfer_resource_repo_$AUTOPAGEXP_DATE`.

## Caution

Please keep in mind the following set of rules while transferring massive projects.

### For issue tickets

-  For projects with huge count of issue tickets available in the source namespace, try breaking down the transfer across multiple merge requests.  
   Have no more than 2000 issue tickets per transfer request.
   ```
   pagure-exporter ... --ranges 1 2000
   ```
   Then reattempt the next 2000 issue tickets on another transfer request.
   ```
   pagure-exporter ... --ranges 2001 4000
   ```
   And so on.

### For repository assets

-  For projects with huge size of repository assets spread across various branches, try breaking down the transfer across multiple merge requests.  
   Have no more than one massive branch per transfer request.
   ```
   pagure-exporter ... --brcs brca
   ```
   Then reattempt the next massive branch on another transfer request.
   ```
   pagure-exporter ... --brcs brcb
   ```
   And so on.

## Appreciation

If the project has been helpful to you, please give stars to the [project repository](https://github.com/fedora-infra/pagure-exporter).

